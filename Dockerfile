FROM node:lts-alpine as build

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx
COPY --from=build /app/dist /app/dist
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf 
