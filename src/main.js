import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

Vue.prototype.$system_url = process.env.VUE_APP_API
Vue.prototype.$system_websocket = process.env.VUE_APP_WebSocket
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
